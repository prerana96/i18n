var Handlebars = require('handlebars');
var i18n = require('i18next');
const path = require('path');

var SyncBackend = require('i18next-sync-fs-backend');

var result;

Handlebars.registerHelper('_', function(i18n_key) {
    var lang = require('../../../api').lang;
    var file = require('../../../api').file;


    i18n
        .use(SyncBackend)
        .init({
            initImmediate: false,
            fallbackLng: 'en',
            getAsync: false,
            load: 'current',
            detectLngQS: 'lang',
            lng: lang,

            ns: ['translation'],

            backend: {
                // path where resources get loaded from
                loadPath: path.join(__dirname, '../../../locales/' + file + '/{{lng}}/{{ns}}.json')
            },

            debug: true,
        });

    result = i18n.t(i18n_key);
    console.log("RESULT:" + result);

    return new Handlebars.SafeString(result);

});